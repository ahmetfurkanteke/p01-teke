//
//  AppDelegate.h
//  project-1
//
//  Created by Ahmet Furkan Teke on 23.01.2017.
//  Copyright © 2017 Ahmet Furkan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

